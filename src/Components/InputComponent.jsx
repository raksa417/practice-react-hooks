import React, { useState } from "react";
import TableComponent from "./TableComponent";
import ageIcon from "../assets/age-icon.png";

function InputComponent({personInfo, setDataPerson}) {
  const [newPerson, setNewPerson] = useState([]);

  const inputHandler = (e) => {
    setNewPerson({ ...newPerson, [e.target.name]: e.target.value });
  };

  const submitHandler = (e) => {
        e.preventDefault();

        setDataPerson([...personInfo,{id: personInfo.length+1,...newPerson, status: "Pending"}])
  }

  return (
    <div>
      <div class="m-auto w-[850px] mt-16">
        <h1 class="text-5xl mb-12 text-center font-extrabold">
          <span class="text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600">
            Please fill your
          </span>
          <span> information</span>
        </h1>

        <form onSubmit={submitHandler}>
          {/* Email */}
          <label
            htmlFor="email"
            class="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
          >
            Email
          </label>
          <div class="flex mb-6">
            <span class="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </span>

            <input
              type="email"
              name="email"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-r-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="raksaruos@gmail.com"
              onChange={inputHandler}
              required
            />
          </div>

          {/* UserName */}
          <label
            htmlFor="userName"
            class="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
          >
            Username
          </label>
          <div class="flex mb-6">
            <span class="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              @
            </span>
            <input
              type="text"
              name="userName"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="raksaruos"
              onChange={inputHandler}
              required
            />
          </div>

          {/* Age */}
          <label
            htmlFor="age"
            class="block mb-2 font-medium text-gray-900 dark:text-white text-xl"
          >
            Age
          </label>
          <div class="flex mb-8">
            <span class="inline-flex items-center px-3 text-lg text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              <img src={ageIcon} alt="ageIcon" style={{ width: "20px" }} />
            </span>

            <input
              type="number"
              name="age"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-r-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="20"
              onChange={inputHandler}
              required
            />
          </div>

          {/* Button */}
          <div class="flex justify-end">
            <button
              type="submit"
              class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-[17px] px-20 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
            >
              Register
            </button>
          </div>
        </form>

        {/* Table for display information */}
        <TableComponent allPersonInfo={personInfo} setDataPersons={setDataPerson}/>
      </div>
    </div>
  );
}

export default InputComponent;
