import React, { useState } from "react";
import InputComponent from "./InputComponent";

const DataComponent = () => {
  const [person, setPerson] = useState([
    {
      id: 1,
      email: "dara@gmail.com",
      userName: "dara",
      age: 20,
      status: "Pending",
    },
    {
      id: 2,
      email: "kaka@gmail.com",
      userName: "kaka",
      age: 30,
      status: "Pending",
    },
    {
      id: 3,
      email: "sopheak@gmail.com",
      userName: "sopheak",
      age: 25,
      status: "Pending",
    },
  ]);

  return(
  <div>
      <InputComponent personInfo={person} setDataPerson={setPerson} />
  </div>
  )
};

export default DataComponent;


