import React, { useState } from "react";
import Swal from "sweetalert2";

const TableComponent = ({ allPersonInfo, setDataPersons }) => {
  const [person, setPerson] = useState([]);
  const [updatePerson, setUpdatePerson] = useState([]);
  const [allInfo,setAllInfo] = useState([]);
 
  const showDetail = (e) => {
    Swal.fire(
      `
            ID : ${e.id}
            Email : ${e.email}
            Username : ${e.userName}
            Age : ${e.age} 
        `
    );
  };

  const changeStatus = (e) => {
        e.status = (e.status === "Pending" ? "Done" : "Pending");
        setUpdatePerson({
          id: e.id,
          email: e.email,
          userName: e.userName,
          age: e.age,
          status: e.status,
        });
        allInfo([...person , ...updatePerson]);
        setDataPersons([...allInfo]);
    };

    return (
      <div>
        <div class="mt-6 w-full">
          <div class="shadow-md sm:rounded-xl">
            <table class="text-center text-gray-500 dark:text-gray-400 w-full">
              <thead class=" text-gray-700 uppercase bg-blue-200 dark:bg-blue-700 dark:text-gray-400 text-[16px]">
                <tr>
                  <th class="px-6 py-3 w-20">ID</th>
                  <th class="w-40">Email</th>
                  <th class="w-32">UserName</th>
                  <th class="w-20">Age</th>
                  <th class="w-60">Action</th>
                </tr>
              </thead>
              <tbody class="text-center text-black">
                {allPersonInfo.map((item) => (
                  <tr
                    key={item.id}
                    class={
                      item.id % 2 === 0
                        ? "bg-pink-100  border-b text-[15px]"
                        : "bg-white  border-b text-[15px]"
                    }
                  >
                    <td class="p-5">{item.id}</td>
                    <td>{item.email}</td>
                    <td>{item.userName}</td>
                    <td>{item.age}</td>
                    <td>
                      <button
                        type="button"
                        class={
                          item.status === "Pending"
                            ? "text-[16px] text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg  w-24 h-10 mr-2 dark:bg-blue-600focus:outline-none dark:focus:ring-blue-800"
                            : "text-[16px] text-white bg-green-600  hover:bg-green-700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg  w-24 h-10 mr-2 dark:bg-blue-600focus:outline-none dark:focus:ring-blue-800"
                        }
                        onClick={() => changeStatus(item)}
                      >
                        {item.status}
                      </button>

                      <button
                        type="button"
                        class="text-[16px] text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg  w-28 h-10  mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                        onClick={() => showDetail(item)}
                      >
                        Show more
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  };

  export default TableComponent;
