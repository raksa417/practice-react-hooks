import React, { Component } from "react";
import DataComponent from "./Components/DataComponent";

export default class App extends Component {
  render() {
    return (
      <div>
        <DataComponent/>
      </div>
    );
  }
}
